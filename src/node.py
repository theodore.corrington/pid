#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32, Bool
from geometry_msgs.msg import Accel
from numpy import linalg as LA
import numpy as np
from tf.transformations import euler_from_quaternion
import math

is_enabled = False

linear = [0, 0, 0]
angular = [0, 0, 0]

target_up_value = 0 # should be a vacuum
target_yaw_value = 0 # should be north
target_pitch_value = 0 # should be level
target_roll_value = 0 # should be level

is_active_up = False
is_active_yaw = False
is_active_pitch = False
is_active_roll = False

const_up_force = 0 # N

def mod(a, n):
    return a - math.floor(a / n) * n
    
def rot_diff(sourceA, targetA):
    diff = targetA - sourceA
    return (diff + 180) % 360 - 180

# functions that perform active correction on a dof

def fix_yaw(data):
    global angular, target_yaw_value, is_active_yaw
    if is_active_yaw:
        angular[2] = max(min(rot_diff(data.data, target_yaw_value) * 0.4, 10), -10)
    else:
        angular[2] = 0 # unlock yaw
    
def fix_pitch(data):
    global angular
    if is_active_pitch:
        angular[0] = max(min(-rot_diff(data.data, target_pitch_value) * 0.3, 20), -20)
    else:
        angular[0] = 0 # unlock pitch
    
def fix_roll(data):
    global angular
    if is_active_roll:
        angular[1] = max(min(-rot_diff(data.data, target_roll_value) * 0.3, 20), -20)
    else:
        angular[1] = 0 # unlock roll

def fix_up(data):
    global linear, target_up_value, is_active_up
    if is_active_up:
        linear[2] = max(min((data.data - target_up_value) * 0.5 + const_up_force, 10), -100)

# functions that set a static force/torque on a dof

def set_force_forward(data):
    global linear
    linear[1] = max(min(data.data, 30), -30)
    
def set_force_right(data):
    global linear
    linear[0] = max(min(data.data, 30), -30)
    
def set_force_up(data):
    global linear
    if not is_active_up:
        linear[2] = max(min(data.data, 10), -100)
    
# functions that set active correction target values
    
def set_target_up(data):
    global target_up_value
    # target pressure must be at least 100000 Pa
    if data.data > 100000:
        target_up_value = data.data

def set_const_up_force(data):
    global const_up_force
    const_up_force = data.data

def set_target_yaw(data):
    global target_yaw_value
    target_yaw_value = data.data

def set_target_pitch(data):
    global target_pitch_value
    target_pitch_value = data.data

def set_target_roll(data):
    global target_roll_value
    target_roll_value = data.data
    
# functions that enable active correction on a dof

def active_up(data):
    global is_active_up, linear
    is_active_up = data.data
    if not is_active_up:
        linear[2] = 0 # unlock up (dive) 
    
def active_yaw(data):
    global is_active_yaw, angular
    is_active_yaw = data.data
    if not is_active_yaw:
        angular[2] = 0 # unlock yaw 

def active_pitch(data):
    global is_active_pitch, angular
    is_active_pitch = data.data
    if not is_active_pitch:
        angular[0] = 0 # unlock pitch 

def active_roll(data):
    global is_active_roll, angular
    is_active_roll = data.data
    if not is_active_roll:
        angular[1] = 0 # unlock roll 
    
# function that enables/disables the node

def enable_node(data):
    global is_enabled, linear, angular
    is_enabled = data.data
    if not is_enabled:
        linear = [0, 0, 0]
        angular = [0, 0, 0]

        target_up_value = 0
        target_yaw_value = 0
        target_pitch_value = 0
        target_roll_value = 0

        is_active_up = False
        is_active_yaw = False
        is_active_pitch = False
        is_active_roll = False

def node():
    global linear, angular, is_enabled
    rospy.init_node('pid')
    
    rospy.Subscriber("/yaw", Float32, fix_yaw, queue_size=1)
    rospy.Subscriber("/pitch", Float32, fix_pitch, queue_size=1)
    rospy.Subscriber("/roll", Float32, fix_roll, queue_size=1)
    
    rospy.Subscriber("/pressure", Float32, fix_up, queue_size=1)
    rospy.Subscriber("/const_up_force", Float32, set_const_up_force, queue_size=1)
    
    rospy.Subscriber("/force_forward", Float32, set_force_forward, queue_size=1)
    rospy.Subscriber("/force_right", Float32, set_force_right, queue_size=1)
    rospy.Subscriber("/force_up", Float32, set_force_up, queue_size=1)

    rospy.Subscriber("/target_up", Float32, set_target_up, queue_size=1)
    rospy.Subscriber("/target_yaw", Float32, set_target_yaw, queue_size=1)
    rospy.Subscriber("/target_pitch", Float32, set_target_pitch, queue_size=1)
    rospy.Subscriber("/target_roll", Float32, set_target_roll, queue_size=1)
    
    rospy.Subscriber("/active_up", Bool, active_up, queue_size=1)
    rospy.Subscriber("/active_yaw", Bool, active_yaw, queue_size=1)
    rospy.Subscriber("/active_pitch", Bool, active_pitch, queue_size=1)
    rospy.Subscriber("/active_roll", Bool, active_roll, queue_size=1)
    
    rospy.Subscriber("/pid_enable", Bool, enable_node, queue_size=1)
    
    accel_pub = rospy.Publisher('/cmd_accel', Accel, queue_size=1)
    
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        if is_enabled:
            a = Accel()
            a.linear.x = linear[0]
            a.linear.y = linear[1]
            a.linear.z = linear[2]
            a.angular.x = angular[0]
            a.angular.y = angular[1]
            a.angular.z = angular[2]
            accel_pub.publish(a)
        rate.sleep()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
